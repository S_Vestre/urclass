<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function school(){
    	return $this->belongsTo('App\School');
    }
    public function users(){
    	return $this->hasMany('App\User');
    }
    public function subjects(){
    	return $this->belongsToMany('App\Subject');
    }
    public function lessons(){
    	return $this->hasMany('App\Lesson');
    }
}
