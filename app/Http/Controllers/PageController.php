<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

class PageController extends Controller
{
    public function dashboard(){
    	$user = Auth::user();
		$school = $user->school;
		$role = $user->role;
		$unusual = false;
		$unusual=true;
		$group = $user->group;
		$lessons = $group->lessons;
		$now = Carbon::now()->startOfDay();
		if(!$now->isWeekday()) $now->addWeekday();
		$weekdays = [];
		//next 5 weekdays
		for($i=0;$i<5;$i++){
			//echo $i." = ".$now." / ".$now->endOfDay()."<br>";
			$weekdays[$i]["start"] = $now->toDateTimeString();
			$weekdays[$i]["end"] = $now->endOfDay()->toDateTimeString();
			setlocale(LC_TIME,"");
			$weekdays[$i]["format"] = $now->formatLocalized('l\\, F jS');
 			$now->startOfDay();
			$now->addWeekday();
		}
		//die(var_dump($lessons));
		return view('dashboard', [
			'user' => [
				'data' => $user,
				'school' => $school,
				'role' => $role,
				'group' => $group
			],
			'lessons' => $lessons,
			'weekdays' => $weekdays
		]);
    }
}
