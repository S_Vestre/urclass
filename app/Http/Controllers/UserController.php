<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserController extends Controller {
	public function SignUp(Request $post){
		$this->validate($post, [
			'email' => 'email|unique:users|required',
			'name' => 'required|max:25',
			'surname' => 'required|max:25',
			'password' => 'required|min:4|confirmed',
			'day' => 'required|max:31|min:1|numeric',
			'month' => 'required|max:12|min:1|numeric',
			'year' => 'required|numeric|between:1900,2016',
			'gender' => ['required', Rule::in([0,1])]
		]);

		$email = $post['email'];
		$password = bcrypt($post['password']);
		$name = $post['name'];
		$surname = $post['surname'];
		$date = $post['year'].'-'.$post['month'].'-'.$post['day'];
		$gender = $post['gender'];

		$user = new User();
		$user->email = $email;
		$user->password = $password;
		$user->name = $name;
		$user->surname = $surname;
		$user->born = $date;
		$user->gender = $gender;
		$user->school_id = 0;
		$user->role_id = 1;
		$user->group_id = 0;

		$user->save();

		return redirect('/')->with('registered', 'You signed up successfully!');
	}

	public function SignIn(Request $post){
		$this->validate($post, [
			'email' => 'email|required',
			'password' => 'required'
		]);

		if(Auth::attempt(['email' => $post['email'],'password' => $post['password']])){
			return redirect()->route('dashboard');
		}
		return redirect()->back();
	}

	public function Logout(){
		Auth::logout();
		return redirect()->route('start');
	}
}