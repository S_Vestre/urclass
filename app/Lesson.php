<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
	protected $dates = ['taking_place_at', 'ending_at'];
    public function group(){
    	return $this->belongsTo('App\Group');
    }
    public function subject(){
    	return $this->belongsTo('App\Subject');
    }
}
