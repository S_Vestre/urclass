<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function groups(){
    	return $this->belongsToMany('App\Group');
    }
    public function school(){
    	return $this->belongsTo('App\School');
    }
    public function teacher(){
    	return $this->belongsTo('App\User','user_id');
    }
}
