<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;
    public function school(){
    	return $this->belongsTo('App\School');
    }
    public function role(){
    	return $this->belongsTo('App\Role');
    }
    public function grades(){
        return $this->hasMany('App\Grade');
    }
    public function group(){
        return $this->belongsTo('App\Group');
    }
    public function subjects(){
    	return $this->hasMany('App\Subject');
    }
    
    public static function role_name($role_name){
    	$real_names = [
    		"student" => "Student",
    		"teacher" => "Teacher",
    		"admin" => "School administration"
    	];
    	return $real_names[$role_name];
    }
}
