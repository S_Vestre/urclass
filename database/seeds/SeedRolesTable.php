<?php

use Illuminate\Database\Seeder;

class SeedRolesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
        	array(
                array('name' => 'guest'),
        		array('name' => 'student'),
        		array('name' => 'teacher'),
        		array('name' => 'admin'),
        		array('name' => 'system_admin')
        	));
    }
}
