<?php

use Illuminate\Database\Seeder;

class SeedSchoolsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schools')->insert(
        	array(
        		array('name' => 'Rīgas Valsts tehnikums'),
        		array('name' => 'Rīgas Tehniskā koledža'),
        		array('name' => 'Rīgas pārtikas ražotāju vidusskola'),
        		array('name' => 'Rīgas Celtniecības koledža'),
        		array('name' => 'Rīgas Dizaina un mākslas vidusskola')
        	));
    }
}
