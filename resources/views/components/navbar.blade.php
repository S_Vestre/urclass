<header>
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">UrClass</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="{{ ((Request::is('dashboard')) ? 'active' : ' ') }}"><a href="/">Sākums</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          @if(!Auth::check())
            <li class="{{ ((Request::is('register')) ? 'active' : ' ') }}"><a href="/register">Reģistrēties</a></li>
            <li class="{{ ((Request::is('/')) ? 'active' : ' ') }}"><a href="/">Ieiet</a></li>
          @else
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mans profils <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Uzstādījumi</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ route('logout') }}">Iziet</a></li>
              </ul>
            </li>
          @endif
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>
</header>