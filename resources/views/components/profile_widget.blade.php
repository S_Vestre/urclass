<div class="my-profile widget col-md-3">
	<div class="user-img" style="background-image: url(images/avatar.png);"></div>
	<h5 class="text-center">{{ $user['data']->name.' '.$user['data']->surname }}</h5>
	<p class="text-center">{{ $user['school']->name or '' }}</p>
	<div class="row">
		<div class="col-xs-{{ ($user['data']->school_id > 0) ? '6' : '12' }}" style="text-align: {{ ($user['data']->school_id > 0) ? 'right' : 'center' }};">{{ App\User::role_name($user['role']->name) }}</div>
		@if($user['data']->school_id > 0)
		<div class="col-xs-6" style="text-align: left;">{{ $user['group']->name }}</div>
		@endif
	</div>
</div>