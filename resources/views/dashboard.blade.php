@extends('sections.master')
@section('title') Dashboard @endsection
@section('content')
<div class="row" style="margin-top:100px;">
	@include('components.profile_widget')
	<div class="main-schedule col-md-6 col-md-offset-1">
		@if($user['data']->school_id>0)
			
			@foreach($weekdays as $key => $weekday)
			<div class="widget row">
				<div class="col-md-12">
					@if($key == 0)
					<div class="row">
						<h3 class="title">Stundu saraksts tuvākajās dienās</h3>
					</div>
					@endif
					<div class="row">
						<div class="lesson">
							<h4 class="day">{{ $weekday['format'] }}</h4>
							<table class="table table-condensed">
								<thead>
									<tr>
										<td>#</td>
										<td>Laiks</td>
										<td>Kabinets</td>
										<td>Priekšmets</td>
										<td>Skolotājs</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$temp = $lessons->where('taking_place_at','>',$weekday['start'])->where('taking_place_at','<',$weekday['end']);
									if(count($temp)>0){
										$i=1;
										foreach ($temp as $key2 => $value) {
											//$dt = Carbon::parse();
											?>
											<tr>
												<td>{{ $i++ }}</td>
												<td>{{ $value->taking_place_at->hour.':'.$value->taking_place_at->minute }} - {{ $value->ending_at->hour.':'.$value->ending_at->minute }}</td>
												<td>{{ $value->classroom }}</td>
												<td>{{ $value->subject->name }}</td>
												<td>{{ $value->subject->teacher->name.' '.$value->subject->teacher->surname }}</td>
											</tr>
											<?php
										}
									}else{
										?>
										<tr>
											<td colspan="6"><p class="text-center">Šajā dienā Tev nav stundu! :)</p></td>
										</tr>
									<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		@else
		<div class="widget row">
			<div class="col-md-12">
				<div class="row">
					<h3>You aren't linked to any school. No dashboard for guests yet.</h3>
				</div>
			</div>
		</div>
		@endif
	</div>

</div>
@endsection