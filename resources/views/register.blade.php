@extends('sections.master')

@section('title') Reģistrēties @endsection

@section('content')
<div class="row">
	<div class="col-md-5 col-md-offset-6">
		<div class="panel sign-in panel-default">
		  	<div class="panel-body">
		  		@if(count($errors) > 0)
					<div class="alert alert-danger" role="alert">
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
		  		@endif
				<form class="form-signin" action="{{ route('SignUp') }}" method="POST">
				    <h2 class="form-signin-heading">Reģistrēties</h2>
					<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
					    <label for="inputEmail" class="sr-only">E-pasts</label>
					    <input type="email" id="inputEmail" name="email" class="form-control" placeholder="E-pasts" required value="{{ Request::old('email') }}" autofocus>
					</div>
					<div class="form-inline">
						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
					   		<label for="inputName" class="sr-only">Vārds</label>
					    	<input type="text" id="inputName" name="name" class="form-control" placeholder="Vārds" value="{{ Request::old('name') }}" required>
					    </div>
					    <div class="form-group {{ $errors->has('surname') ? 'has-error' : '' }}">
						    <label for="inputSurname" class="sr-only">Uzvārds</label>
						    <input type="text" id="inputSurname" name="surname" class="form-control" placeholder="Uzvārds" value="{{ Request::old('surname') }}" required>
						</div>
					</div>
					<div class="form-inline">
						<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
						    <label for="inputPassword" class="sr-only">Parole</label>
						    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Parole" required>
						</div>
						<div class="form-group">
						    <label for="inputPassword2" class="sr-only">Parole atkārtoti</label>
						    <input type="password" id="inputPassword2" name="password_confirmation" class="form-control" placeholder="Parole atkārtoti" required>
						</div>
					</div>
					<div class="form-inline">
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control" style="width:33.3%" id="exampleInputAmount" placeholder="DD" value="{{ Request::old('day') }}" name="day">
								<input type="text" class="form-control" style="width:33.3%" id="exampleInputAmount" placeholder="MM" value="{{ Request::old('month') }}" name="month">
								<input type="text" class="form-control" style="width:33.3%" id="exampleInputAmount" placeholder="GGGG" value="{{ Request::old('year') }}" name="year">
							</div>
						</div>
						<div class="form-group">
							<select class="form-control has-placeholder" name="gender">
							  <option selected disabled>Dzimums</option>
							  <option value="0">Vīrietis</option>
							  <option value="1">Sieviete</option>
							</select>
						</div>
					</div>
				    <div class="form-group">
				    	<button class="btn btn-primary" type="submit">Reģistrēties</button>
				    	<input type="hidden" name="_token" value="{{ Session::token() }}" />
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection