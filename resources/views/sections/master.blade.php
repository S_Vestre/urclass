<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UrClass | @yield('title')</title>
        
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    </head>
    <body>
        @include('components.navbar')
        <div class="container">
            @yield('content')
        </div>
        
        <script src="{{ URL::asset('js/app.js') }}"></script>
    </body>
</html>
