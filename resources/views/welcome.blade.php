@extends('sections.master')

@section('title') Ieiet @endsection

@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="panel sign-in panel-default">
		  	<div class="panel-body">
		  		@if (session('registered'))
		  			<div class="alert alert-success" role="alert">{{ session('registered') }}</div>
		  		@endif
		  		@if(count($errors) > 0)
					<div class="alert alert-danger" role="alert">
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
		  		@endif
				<form action="{{ route('SignIn') }}" method="post" class="form-signin">
				    <h2 class="form-signin-heading">Autorizācija</h2>
				    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
					    <label for="inputEmail" class="sr-only">E-pasts</label>
					    <input type="email" id="inputEmail" name="email" class="form-control" placeholder="E-pasts" value="{{ Request::old('email') }}" required autofocus>
					</div>
					<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
					    <label for="inputPassword" class="sr-only">Parole</label>
					    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Parole" required>
					</div>
				    <div class="checkbox">
				      <label>
				        <input type="checkbox" name="remember" value="remember-me"> Atcerēties mani
				      </label>
				    </div>
				    <button class="btn btn-primary" type="submit">Ieiet</button>
				    <input type="hidden" name="_token" value="{{ Session::token() }}" />
				</form>
			</div>
		</div>
	</div>
</div>
@endsection