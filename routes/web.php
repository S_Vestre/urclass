<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('start')->middleware('guest');

Route::get('/register', function (){
	return view('register');
})->middleware('guest');

Route::post('/register', 'UserController@SignUp')->name('SignUp')->middleware('guest');
Route::post('/signin', 'UserController@SignIn')->name('SignIn')->middleware('guest');
Route::get('/logout', 'UserController@Logout')->name('logout');

Route::get('/dashboard', 'PageController@dashboard')->name('dashboard')->middleware('auth');